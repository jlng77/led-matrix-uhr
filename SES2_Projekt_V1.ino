#include "LedControl.h"

/*
 Die Pinbelegung ist folgende:
 DataIn = pin 14
 CS = pin 15
 CLK = pin 16
 */
LedControl lc = LedControl(14, 16, 15, 4);

const int ROWS = 8;
const int COLUMN = 8;

byte numbers[10][8] = {
//Null
{B00011000,
 B00100100,
 B00100100,
                   B00100100,
                   B00100100,
                   B00100100,
                   B00100100,
                   B00011000 },
//Eins
                   { B00000000,
                  B00010000,
                  B00010000,
                  B00010000,
                  B00010000,
                  B00010000,
                  B00010000,
                  B00000000 },
//Zwei
{ B00011000,
                  B00100100,
                  B00000100,
                  B00001000,
                  B00010000,
                  B00100000,
                  B00111100,
                  B00000000 },
//Drei
{ B00111000,
                    B00000100,
                    B00000100,
                    B00111000,
                    B00000100,
                    B00000100,
                    B00111000,
                    B00000000 },
//Vier
{ B00000000,
                   B00000100,
                   B00001100,
                   B00010100,
                   B00111110,
                   B00000100,
                   B00000100,
                   B00000000 },
//Fuenf
{ B00111100,
                   B00100000,
                   B00100000,
                   B00111100,
                   B00000100,
                   B00000100,
                   B01000100,
                   B00111100 },
//Sechs
{ B00111000,
                  B00100000,
                  B00100000,
                  B00111100,
                  B00100100,
                  B00100100,
                  B00100100,
                  B00111100 },
//Sieben
{ B00111110,
                    B00000110,
                    B00001100,
                    B00011000,
                    B00110000,
                    B00110000,
                    B00110000,
                    B00000000 },
//Acht
{ B00011100,
                    B00100100,
                    B00100100,
                    B00011000,
                    B00100100,
                    B00100100,
                    B00100100,
                    B00011000 },
//Neun
{ B00111100,
                   B00100100,
                   B00100100,
                   B00111100,
                   B00000100,
                   B00000100,
                   B00000100,
                   B00011100 }

};


/* Kurze Wartezeit beim aktualisieren der Matrix */
unsigned long delaytime = 100;

void setup() {
  /*Die Matrix starten*/
  lc.shutdown(0, false);
  /* Helligkeit einstellen */
  lc.setIntensity(0, 8);
  /* Alle LEDs der Matirx ausschalten */
  lc.clearDisplay(0);
}

void writeNumber(int matrix, int number){
  for (int i=0; i<ROWS;i++){
    lc.setRow(matrix, i, numbers[number][i]);
  }
}
void loop() {
  for(int i=0;i<=9;i++){
    writeNumber(0,i);
    delay(1000);
  }
}

void currentTime(){

}